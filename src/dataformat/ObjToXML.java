package dataformat;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ObjToXML {

	public static void main(String[] args) throws Exception {
		
		String objFile = args[0];
		String xmlFile = args[1];
		
		BufferedReader objReader = new BufferedReader(new FileReader(objFile));
		BufferedWriter xmlWriter = new BufferedWriter(new FileWriter(xmlFile));
       
		String line;
		
		StringBuilder vertices = new StringBuilder();
		StringBuilder indices = new StringBuilder();
		
        while((line = objReader.readLine()) != null)
        {
        	String[] typeAndValue = line.split(" ", 2);
        	String type = typeAndValue[0];
        	String value = typeAndValue[1];
        	
        	if(type.equals("v"))
        	{
        		vertices.append(value);
        		vertices.append(",");
        	}
        	else if(type.equals("f"))
        	{
        		String[] vertex = value.split(" ");
        		
        		for(int i = 0; i < 3; i++)
        		{
	        		String[] fields = vertex[i].split("/");
	        		indices.append(fields[0]);
	        		indices.append(",");
        		}
        	}
        	else
        	{
        		//throw new Exception("Geen Type");
        	}
        }
        
        vertices.deleteCharAt(vertices.length() - 1);
        indices.deleteCharAt(indices.length() - 1);
        
        xmlWriter.write("<IndexedTriangleSet ");
        
        xmlWriter.write("coordinates=\"");
        xmlWriter.write(vertices.toString());
        xmlWriter.write("\" ");
        
        xmlWriter.write("coordinateIndices=\"");
        xmlWriter.write(indices.toString());
        xmlWriter.write("\" ");
        
        xmlWriter.write("name=\"auto\" />");
        
        xmlWriter.flush();
        
        
        xmlWriter.close();
        objReader.close();
	}
	
}
