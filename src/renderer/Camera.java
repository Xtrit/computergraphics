package renderer;

import java.awt.RadialGradientPaint;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import renderer.geometry.BoundingBox;

import util.Point2f;
import util.Tuple2f;
import util.Tuple3f;

public class Camera
{

	public Camera(Tuple2f resolution, Tuple3f position, Tuple3f direction, float fovy, Tuple3f up)
	{
		
		
		this.position = position;
		this.direction = direction;
		this.resolution = resolution;
		this.up = up;
		this.right = direction.cross(up);
		
		spacing = (float) (Math.tan(Math.toRadians(fovy) / 2)/resolution.y) * 2;
		pointOfCenter = new Tuple2f((resolution.x / 2) - 0.5f, (resolution.y / 2) - 0.5f);

	}
	private Tuple2f pointOfCenter;
	private Tuple3f position;
	private Tuple3f direction;
	
	private Tuple3f up;
	private Tuple3f right;
	
	private float spacing;
	
	private Tuple2f resolution;
	
	public List<Ray> createRays()
	{
		throw new NotImplementedException(); //TODO Implement
	}
	
	public Ray createRayForPixel(Tuple2f pixel)
	{
		
		/*
		 * Tuple2f offsetFromCenter = pixel - pointOfCenter
		 * Vector2f rayDirection = direct + offsetFromCenter.x*spacing*right + offsetFromCenter.y*spacing*up
		 */
		Tuple2f offsetFromCenter = pixel.min(pointOfCenter);
		
		Tuple3f rayDirection = direction.add(right.mul(offsetFromCenter.x*spacing)).add(up.mul(offsetFromCenter.y*spacing)); 
		
		Ray ray = new CameraRay(position, rayDirection.normalize(), pixel);
		return ray;
		//throw new NotImplementedException(); //TODO Implement

	}

	public List<Ray> createRandomRaysForPixel(Tuple2f pixel, int size)
	{
		List<Ray> rays = new ArrayList<Ray>(size);
		Random random = new Random();
		/*
		 * Tuple2f offsetFromCenter = pixel - pointOfCenter
		 * Vector2f rayDirection = direct + offsetFromCenter.x*spacing*right + offsetFromCenter.y*spacing*up
		 */
		for(int i = 0; i < size; i++)
		{
			Tuple2f offsetFromCenter = pixel.min(pointOfCenter);
			
			float divX = (float) (random.nextFloat() - 0.5);
			float divY = (float) (random.nextFloat() - 0.5);
			
			offsetFromCenter.x = offsetFromCenter.x + divX;
			offsetFromCenter.y = offsetFromCenter.y + divY;
			
			Tuple3f rayDirection = direction.add(right.mul(offsetFromCenter.x*spacing)).add(up.mul(offsetFromCenter.y*spacing)); 
			
			Ray ray = new CameraRay(position, rayDirection.normalize(), pixel);
			
			rays.add(ray);
		}
		return rays;
		//throw new NotImplementedException(); //TODO Implement

	}
	
	public Tuple3f getPosition() {
		return position;
	}
	
	
}
