package renderer;

import java.util.HashMap;
import java.util.Map;

import renderer.scene.Builder;
import util.Point3f;
import util.Tuple2f;
import util.Tuple3f;

public class CameraBuilder extends Builder<Camera> {
	
	public Camera create(Tuple2f resolution, Tuple3f position, Tuple3f direction, Tuple3f up, float fovy, String name)
	{
		//throw new NotImplementedException(); //TODO Implement
		Camera camera = new Camera(resolution, position, direction, fovy,up);
		register(camera, name);
		
		return camera;
	}
}
