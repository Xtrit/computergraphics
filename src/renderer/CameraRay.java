package renderer;

import util.Tuple2f;
import util.Tuple3f;

public class CameraRay extends Ray {

	public CameraRay(Tuple3f origin, Tuple3f direction, Tuple2f coordinateScreen) {
		super(origin, direction, coordinateScreen);
	}

}
