package renderer;

import renderer.geometry.Renderable;
import util.Tuple3f;

public class CollisionPoint {
	private Tuple3f collisionPoint;

	public CollisionPoint(Tuple3f collisionPoint, Renderable renderable, Tuple3f barycentricCoorindates, float distance, Tuple3f normal) {
		this.collisionPoint = collisionPoint;
		this.renderable = renderable;
		this.bary = barycentricCoorindates;
		this.distance = distance;
		this.normal = normal;
	}

	public Tuple3f getCollisionPoint()
	{
		return collisionPoint;
	}
	
	public Renderable getRenderable()
	{
		return renderable;
	}
	
	private Renderable renderable;
	
	public Tuple3f getBarycentricCoordinates()
	{
		return bary;
	}
	
	private Tuple3f bary;
	
	public float getDistance()
	{
		return distance;
	}
	
	private float distance;

	private Tuple3f normal;
	
	public Tuple3f getNormal() {
		return normal;
	}
}
