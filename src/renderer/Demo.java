package renderer;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;

import renderer.collision.CollisionDetector;
import renderer.collision.CompactGrid;
import renderer.collision.SimpleCollisionDetector;
import renderer.scene.Scene;
import renderer.scene.SceneBuilder;
import util.CgPanel;
import util.Color3f;
import util.Tuple2f;
import util.Tuple3f;

public class Demo implements MouseListener {

public static void main(String[] args) {
	new Demo("./example.sdl", new String[0][]);
}

private JFrame frame;
private CgPanel panel;

public Demo(String file, String[][] params) {
		
	panel = new CgPanel();
	panel.addMouseListener(this);
	frame = new JFrame();
	frame.setSize(640, 480);
	frame.getContentPane().add(panel);
	frame.setVisible(true);
	
	
	
	loadFile(file, params);
}

public void mousePressed(MouseEvent e) { }
public void mouseClicked(MouseEvent e) { timePerformance(); }
public void mouseEntered(MouseEvent e) { }
public void mouseExited(MouseEvent e) { }
public void mouseReleased(MouseEvent e) { }

private Renderer renderer;

public void loadFile(String file, String[][] params)
{
	Scene scene = null;
	
	try {
		SceneBuilder sceneBuilder = new SceneBuilder();
		scene = sceneBuilder.loadScene(file, new Tuple2f(panel.getWidth(), panel.getHeight()));
	}
	catch (Exception e) {		
		throw new RuntimeException(e);
	}

	CollisionDetector cd = new SimpleCollisionDetector(scene.getRenderables());

	//cd = new CompactGrid(scene,1f/10f);

	
	
	renderer = new Renderer(scene, cd);
	System.out.println("GO");
}

private boolean trueColor = false;

public void drawPixels() {
	
	
	
	Color3f[][] color = new Color3f[panel.getHeight()][panel.getWidth()];
	Statistics[][] statistics = new Statistics[panel.getHeight()][panel.getWidth()];
	
	int maxIntersections = 0;
	
	int intersections = 0;
	
	for (int y=0; y<panel.getHeight(); y++) {
		for (int x=0; x<panel.getWidth(); x++) {			
			statistics[y][x] = new Statistics();
			color[y][x] = renderer.renderPixle(new Tuple2f(x, y), statistics[y][x], 0);
			
			intersections += statistics[y][x].getIntersectionTests();
			if(statistics[y][x].getIntersectionTests() > maxIntersections)
			{
				maxIntersections = statistics[y][x].getIntersectionTests();
			}
			//panel.drawPixel(x, panel.getHeight() - y - 1, color.x, color.y, color.z);
		}
	}
	
	System.out.println("TotIntersections: " + intersections);
	System.out.println("MaxIntersections: " + maxIntersections);
	
	for (int y=0; y<panel.getHeight(); y++) {
		for (int x=0; x<panel.getWidth(); x++) {
			Color3f clr;
			if(trueColor)
				clr = color[y][x];
			else
				clr = statistics[y][x].getIntersectionColor(maxIntersections);
			
			panel.drawPixel(x, panel.getHeight() - y - 1, clr.x, clr.y, clr.z);
			
				
		}
	}
	
	
}


public void timePerformance() {
	System.out.println("Start");
	long starttime = System.currentTimeMillis();
	//for (int i=0; i<100; i++) {
		panel.clear(0,0,1);
		drawPixels();
		panel.repaint();
		panel.flush();
	//}
	long endtime = System.currentTimeMillis();
	double fps = 1000.0/((double)(endtime-starttime)/*100.0*/);
	System.out.println("Fps: " + fps);
	panel.saveImage("image.png");
}
}
