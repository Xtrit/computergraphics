package renderer;

import util.Point2f;
import util.Tuple2f;
import util.Tuple3f;
import util.Vector3f;

public class Ray
{
	private Tuple3f origin;
	private Tuple3f direction;
	private Tuple2f coordinateScreen;

	public Ray(Tuple3f origin, Tuple3f direction, Tuple2f coordinateScreen)
	{
		this.origin = origin;
		this.direction = direction;
		this.coordinateScreen = coordinateScreen;
	}
	
	public Tuple3f getOrigin()
	{
		return origin;
	}
	
	public Tuple3f getDirection()
	{
		return direction;
	}
	
	public Tuple2f getCoordinateScreen()
	{
		return coordinateScreen;
	}
	
	public Tuple3f getPositionWithT(float t)
	{
		return origin.add(direction.mul(t));
	}
}
