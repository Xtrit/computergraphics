package renderer;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import renderer.collision.CollisionDetector;
import renderer.geometry.Renderable;
import renderer.scene.Scene;
import util.Color3f;
import util.Tuple2f;
import util.Tuple3f;

public class Renderer {

	public Renderer(Scene scene, CollisionDetector collisionDetector/*, boolean trueColor*/)
	{
		this.scene = scene;
		this.collisionDetector = collisionDetector;
		//this.trueColor = trueColor;
		
		//throw new NotImplementedException(); //TODO Implement
	}
	
	private Scene scene;
	private CollisionDetector collisionDetector;
	
	
	public Color3f renderRay(Ray ray, Statistics statistics, int depth, boolean inMaterial, Renderable exclude)
	{
		
		
		float t = Float.POSITIVE_INFINITY;
		
		//Statistics statistics = new Statistics();
		CollisionPoint closestCollision = getCollision(ray, t, statistics, exclude);
		
		if(closestCollision == null || depth == 0)
		{
			return scene.getBackgroundColor();
		}
	
		return closestCollision.getRenderable().getObject().handleCollision(closestCollision, scene.getLights(), this, ray, depth, inMaterial, statistics);
	
	}

	public CollisionPoint getCollision(Ray ray, float t, Statistics statistics, Renderable exclude)
	{
		return getCollision(ray, t, false, exclude, statistics);
	}
	
	public CollisionPoint getCollision(Ray ray, float t, boolean returnAtAnyCollision, Renderable exclude, Statistics statistics) {
		return collisionDetector.getClosestCollision(ray, t, returnAtAnyCollision, exclude, statistics);
	}
	
	public Color3f renderPixle(Tuple2f coordinate, Statistics statistics, int aa)
	{
		if(coordinate.x == 340 && coordinate.y == 250)
			System.out.println("");
		
		int depth = 100;
		if(aa == 0)
		{
			Ray ray = scene.getCamera().createRayForPixel(coordinate);
			return renderRay(ray, statistics, depth, false, null);
		}
		
		
		
		List<Ray> rays = scene.getCamera().createRandomRaysForPixel(coordinate, aa);
		
		Tuple3f color = new Tuple3f();
		
		for(Ray ray: rays)
		{
			color =  color.add(renderRay(ray, statistics, depth, false, null).div(aa));
		}
		
		return new Color3f(color);
	}
}
