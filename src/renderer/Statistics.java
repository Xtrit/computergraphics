package renderer;

import util.Color3f;

public class Statistics {
	public void increaseIntersectionTests()
	{
		intersectionTests++;
	}
	
	private int intersectionTests  = 0;
	
	public int getIntersectionTests()
	{
		return intersectionTests;
	}

	public Color3f getIntersectionColor(int maxIntersections) {
		float intersections = getIntersectionTests();
		return new Color3f(intersections/maxIntersections,(maxIntersections - intersections)/maxIntersections, 0);
	}
}
