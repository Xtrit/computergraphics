package renderer.collision;

import java.util.ArrayList;
import java.util.List;

import renderer.CollisionPoint;
import renderer.NotImplementedException;
import renderer.Ray;
import renderer.Statistics;
import renderer.geometry.Renderable;

public class BVHBranch extends BVHNode
{
	private BVHNode left;
	private BVHNode right;
	private Plane planeL;
	private Plane planeR;
	
	@Override
	public CollisionPoint getClosestCollision(Ray ray, float tMin, float tMax,
			boolean returnAtAnyCollision, Renderable exclude,
			Statistics statistics) {
		
		float tL = planeL.getCollisionT(ray);
		float tR = planeR.getCollisionT(ray);
		
		List<CollisionPoint> pairs = new ArrayList<CollisionPoint>();
		
		if(tL == Float.POSITIVE_INFINITY || tL == Float.NEGATIVE_INFINITY)
		{
			if(ray.getOrigin().get(planeL.getComponent()) < planeL.getCoordinate())
				pairs.add(left.getClosestCollision(ray, tMin, tMax, returnAtAnyCollision, exclude, statistics));
			
			if(ray.getOrigin().get(planeR.getComponent()) > planeR.getCoordinate())
				pairs.add(right.getClosestCollision(ray, tMin, tMax, returnAtAnyCollision, exclude, statistics));
				
			if(pairs.size() == 1)
				return pairs.get(0);
			
			if(pairs.get(0).getDistance() < pairs.get(1).getDistance())
				return pairs.get(0);
			else
				return pairs.get(1);
		}
		
		throw new NotImplementedException(); //TODO Implement
	}

}
