package renderer.collision;

import java.util.ArrayList;

import renderer.CollisionPoint;
import renderer.Ray;
import renderer.Statistics;
import renderer.geometry.Renderable;

public class BVHLeaf extends BVHNode{

	public BVHLeaf()
	{
		scd = new SimpleCollisionDetector(new ArrayList<Renderable>());
	}
	
	
	
	private SimpleCollisionDetector scd;
	
	@Override
	public CollisionPoint getClosestCollision(Ray ray, float tMin, float tMax,
			boolean returnAtAnyCollision, Renderable exclude,
			Statistics statistics) {
		return scd.getClosestCollision(ray, tMax, returnAtAnyCollision, exclude, statistics);
		//return new TComponentPair(t, component)
	}

}
