package renderer.collision;

import renderer.CollisionPoint;
import renderer.Ray;
import renderer.Statistics;
import renderer.geometry.Renderable;

public abstract class BVHNode {
	
	
	
	public abstract CollisionPoint getClosestCollision(Ray ray, float tMin, float tMax,
			boolean returnAtAnyCollision, Renderable exclude,
			Statistics statistics);
	
}
