package renderer.collision;

import java.util.List;

import renderer.CollisionPoint;
import renderer.Ray;
import renderer.Statistics;
import renderer.geometry.Renderable;

public class BoundingVolumeHierarchy extends CollisionDetector{

	private BVHBranch root;
	
	public BoundingVolumeHierarchy(List<Renderable> renderables)
	{
		root = new BVHBranch();
	}
	
	@Override
	public CollisionPoint getClosestCollision(Ray ray, float t,
			boolean returnAtAnyCollision, Renderable exclude,
			Statistics statistics) {
		return root.getClosestCollision(ray, 0, t, returnAtAnyCollision, exclude, statistics);
	}

}
