package renderer.collision;

import java.util.Iterator;

import renderer.CollisionPoint;
import renderer.Ray;
import renderer.Statistics;
import renderer.geometry.Renderable;

public abstract class CollisionDetector {

	public abstract CollisionPoint getClosestCollision(Ray ray, float t, boolean returnAtAnyCollision, Renderable exclude, Statistics statistics);
}
