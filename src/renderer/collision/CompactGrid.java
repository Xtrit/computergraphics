package renderer.collision;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import renderer.CollisionPoint;
import renderer.NotImplementedException;
import renderer.Ray;
import renderer.Statistics;
import renderer.geometry.BoundingBox;
import renderer.geometry.Renderable;
import renderer.scene.Scene;
import util.Tuple3f;

public class CompactGrid extends CollisionDetector{
	
	private BoundingBox box;
	private Tuple3f space;
	private int boxes;
	
	private Cell[][][] cells;
	
	public CompactGrid(Scene scene, float averageNbObjectInCell)
	{
		List<Renderable> renderables = scene.getRenderables();
		
		box = new BoundingBox();
		space = new Tuple3f();
		
		for(Renderable renderable: renderables)
		{
			box.expand(renderable.calculateBoundingBox());
		}
		
		box.expand(scene.getCamera().getPosition());
		
		float nbOfCells = (float) Math.pow(renderables.size()/averageNbObjectInCell, 1f/3f);
		
		//A extra border because points on a line are also put in the next cell
		boxes = (int) Math.round(nbOfCells) + 1;
		
		for(Component component: Component.values())
		{	
			float length = box.getLength(component);
			
			float spaceForComponent = length / (boxes - 1);
			component.setComponent(space, spaceForComponent);
		}
		
		cells = new Cell[boxes][boxes][boxes];
		
		for(int x = 0; x < boxes; x++)
			for(int y = 0; y < boxes; y++)
				for(int z = 0; z < boxes; z++)
				{
					cells[x][y][z] = new Cell();
					
					int[] xyz = {x, y, z};
					
					for(Component c: Component.values())
					{
						float coordinateMax = box.getValMin(c) + space.get(c) * (c.getComponent(xyz) + 1);
						float coordinateMin = box.getValMin(c) + space.get(c) * (c.getComponent(xyz));
						
						cells[x][y][z].setPlane(c, true, coordinateMax);
						cells[x][y][z].setPlane(c, false, coordinateMin);
					}
					
					
					if(x != 0)
					{
						cells[x][y][z].setNeighbour(Component.X, false, cells[x - 1][y][z]);
						cells[x - 1][y][z].setNeighbour(Component.X, true, cells[x][y][z]);
					}
					
					if(y != 0)
					{
						cells[x][y][z].setNeighbour(Component.Y, false, cells[x][y - 1][z]);
						cells[x][y - 1][z].setNeighbour(Component.Y, true, cells[x][y][z]);
					}
					
					if(z != 0)
					{
						cells[x][y][z].setNeighbour(Component.Z, false, cells[x][y][z - 1]);
						cells[x][y][z - 1].setNeighbour(Component.Z, true, cells[x][y][z]);
					}
				}
		
		for(Renderable renderable: renderables)
		{
			addRenderableToCells(renderable);
		}
		
		return;
	}

	
	private void addRenderableToCells(Renderable renderable)
	{				
		int[] indexMin = new int[3];
		int[] indexMax = new int[3];
		
		BoundingBox rendBox = renderable.calculateBoundingBox();
		
		for(Component c: Component.values())
		{
			
			int minIndex = (int) Math.floor((rendBox.getValMin(c) - box.getValMin(c)) / c.getComponent(space));
			int maxIndex = (int) Math.ceil((rendBox.getValMax(c) - box.getValMin(c)) / c.getComponent(space));
			
			//System.out.println(c + ": " + renderable + " in " + minIndex + " tot " + maxIndex);
			
			c.setComponent(indexMin, minIndex);
			c.setComponent(indexMax, maxIndex);
		}
		
		for(int indexX = indexMin[0]; indexX <= indexMax[0]; indexX++)
			for(int indexY = indexMin[1]; indexY <= indexMax[1]; indexY++)
				for(int indexZ = indexMin[2]; indexZ <= indexMax[2]; indexZ++)
				{
					//System.out.println(renderable + " in " + indexX + ", " + indexY + ", " + indexZ);
					cells[indexX][indexY][indexZ].renderables.add(renderable);
				}
	}

	private static class Cell
	{
		
		public List<Renderable> renderables = new ArrayList<Renderable>();
		
		private Cell[] neighbours = new Cell[2 * Component.values().length];
		private BoundingBox box = new BoundingBox();
		
		public Cell getNeighbour(Component component, boolean positive)
		{
			if(positive)
				return neighbours[2 * component.ordinal() + 1];
			else
				return neighbours[2 * component.ordinal()];
		}
		
		public void setNeighbour(Component component, boolean positive, Cell neighbour)
		{
			if(positive)
				neighbours[2 * component.ordinal() + 1] = neighbour;
			else
				neighbours[2 * component.ordinal()] = neighbour;
		}
		
		public void setPlane(Component component, boolean positive, float coordinate)
		{
			if(positive)
			{
				box.setMax(component, coordinate);
			}
			else
			{
				box.setMin(component, coordinate);
			}
		}
	}


	@Override
	public CollisionPoint getClosestCollision(Ray ray, float t, boolean returnAtAnyCollision, Renderable exclude, Statistics statistics) {
		
		Cell currentCell;
		int[] cell = new int[Component.values().length];
		
		for(Component c: Component.values())
		{
			cell[c.ordinal()] = (int) ((ray.getOrigin().get(c) - box.getValMin(c)) / space.get(c));
		}

		try
		{
			currentCell = cells[cell[0]][cell[1]][cell[2]];
		}
		catch(Exception e)
		{
			throw new RuntimeException();
		}
		
		while(true)
		{
			TComponentPair tCPair = currentCell.box.getClosestCollisionFromWithin(ray);
			
			TComponentPair smallest;
			
			if(tCPair.t > t)
				smallest = new TComponentPair(t, null);
			else
				smallest = tCPair;
			
			SimpleCollisionDetector scd = new SimpleCollisionDetector(currentCell.renderables);
			
			CollisionPoint collision = scd.getClosestCollision(ray, smallest.t, returnAtAnyCollision, exclude, statistics);
			
			//Collision found
			if(collision != null)
				return collision;
			
			//Given T is reached
			if(smallest.component == null)
				return null;
			
			//Traveling if possible
			Cell neighbour = currentCell.getNeighbour(smallest.component, ray.getDirection().get(smallest.component) > 0);
			
			if(neighbour == null)
				return null;
			
			currentCell = neighbour;
		}
	}
	
	
}

