package renderer.collision;

import renderer.geometry.BoundingBox;
import util.Tuple3f;

public enum Component
{
	X {
		

		@Override
		public float getComponent(Tuple3f tuple) {
			return tuple.x;
		}

		@Override
		public void setComponent(Tuple3f tuple, float value)
		{
			tuple.x = value;
		}
		
		@Override
		public int getComponent(int[] ints) {
			return ints[0];
		}
		
		@Override
		public void setComponent(int[] ints, int value)
		{
			ints[0] = value;
		}
	},
	Y {
		

		@Override
		public float getComponent(Tuple3f tuple) {
			return tuple.y;
		}

		@Override
		public void setComponent(Tuple3f tuple, float value)
		{
			tuple.y = value;
		}
		
		@Override
		public int getComponent(int[] ints) {
			return ints[1];
		}
		
		@Override
		public void setComponent(int[] ints, int value)
		{
			ints[1] = value;
		}
	},
	Z {
		

		@Override
		public float getComponent(Tuple3f tuple) {
			return tuple.z;
		}

		@Override
		public void setComponent(Tuple3f tuple, float value)
		{
			tuple.z = value;
		}
		
		@Override
		public int getComponent(int[] ints) {
			return ints[2];
		}
		
		@Override
		public void setComponent(int[] ints, int value)
		{
			ints[2] = value;
		}
	};
	
	
	public abstract void setComponent(int[] ints, int value);
	public abstract void setComponent(Tuple3f tuple, float value);
	
	public abstract float getComponent(Tuple3f tuple);
	public abstract int getComponent(int[] ints);
}
