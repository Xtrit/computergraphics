package renderer.collision;

import renderer.Ray;
import util.Tuple3f;

public class Plane {

	private float coordinate;
	private Component component;
	
	public Plane(float coordinate, Component component)
	{
		this.component = component;
		this.coordinate = coordinate;
	}
	
	public float getCollisionT(Ray ray)
	{
		return (coordinate - component.getComponent(ray.getOrigin()))/component.getComponent(ray.getDirection());
	}

	public float getCoordinate() {
		return coordinate;
	}
	
	public void setCoordinate(float coordinate)
	{
		this.coordinate = coordinate;
	}
	
	public Component getComponent()
	{
		return component;
	}
	
}
