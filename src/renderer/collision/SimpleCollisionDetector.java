package renderer.collision;

import java.util.List;

import renderer.CollisionPoint;
import renderer.Ray;
import renderer.Statistics;
import renderer.geometry.Renderable;

public class SimpleCollisionDetector extends CollisionDetector {

	
	
	public SimpleCollisionDetector(List<Renderable> renderables) {
		this.renderables = renderables;
	}

	private List<Renderable> renderables;





	@Override
	public CollisionPoint getClosestCollision(Ray ray, float t, boolean returnAtAnyCollision, Renderable exclude, Statistics statistics) {
		CollisionPoint closestCollision = null;
		
		for(Renderable renderable: renderables)
		{
			statistics.increaseIntersectionTests();
			
			if(renderable == exclude)
				continue;
			
			CollisionPoint collision = renderable.getCollision(ray, t);
			
			if(collision != null && collision.getDistance() != 0f)
			{
				closestCollision = collision;
				t = collision.getDistance();
				
				if(returnAtAnyCollision)
					return closestCollision;
			}
		}
		
		
		return closestCollision;
	}
	

}
