package renderer.geometry;



import renderer.Ray;
import renderer.collision.Component;
import renderer.collision.Plane;
import renderer.collision.TComponentPair;
import util.Tuple3f;

public class BoundingBox {

	Plane[] planes;

	public BoundingBox() {
		planes = new Plane[2 * Component.values().length];
		
		for(Component c: Component.values())
		{
			planes[2 * c.ordinal()] = new Plane(0, c);
			planes[2 * c.ordinal() + 1] = new Plane(0, c);
		}
	}

	public Plane getMin(Component c)
	{
		return planes[2 * c.ordinal()];
	}
	
	public Plane getMax(Component c)
	{
		return planes[2 * c.ordinal() + 1];
	}
	
	
	public void expand(BoundingBox otherBox)
	{
		for(Component c: Component.values())
		{
			expandMin(otherBox.getMin(c).getCoordinate(), c);
			expandMax(otherBox.getMax(c).getCoordinate(), c);
		}
	}
	
	public void expand(Tuple3f point)
	{
		for(Component c: Component.values())
		{
			expand(c.getComponent(point), c);
		}
	}
	
	public void expand(float coordinate, Component c)
	{
		expandMin(coordinate, c);
		expandMax(coordinate, c);
	}
	
	public void expandMin(float coordinate, Component c)
	{
		Plane plane = planes[2 * c.ordinal()];
		
		if(plane.getCoordinate() > coordinate)
			plane.setCoordinate(coordinate);
	}
	
	public void expandMax(float coordinate, Component c)
	{
		Plane plane = planes[2 * c.ordinal() + 1];
		
		if(plane.getCoordinate() < coordinate)
			plane.setCoordinate(coordinate);
	}

	public void setMin(Tuple3f minAll) {
		for(Component c: Component.values())
		{
			planes[2 * c.ordinal()] = new Plane(c.getComponent(minAll), c);
		}
	}
	
	public void setMax(Tuple3f minAll) {
		for(Component c: Component.values())
		{
			planes[2 * c.ordinal() + 1] = new Plane(c.getComponent(minAll), c);
		}
	}
	
	public void setMin(Component c, float coordinate)
	{
		planes[2 * c.ordinal()] = new Plane(coordinate, c);
	}
	
	public void setMax(Component c, float coordinate)
	{
		planes[2 * c.ordinal() + 1] = new Plane(coordinate, c);
	}
	
	public float getVolume()
	{
		float volume = 1f;
		
		for(Component c: Component.values())
		{
			volume *= getLength(c);
		}
		
		return volume;
	}
	
	public float getLength(Component c)
	{
		return planes[2 * c.ordinal() + 1].getCoordinate() - planes[2 * c.ordinal()].getCoordinate();
	}

	public float getValMin(Component c) {
		return getMin(c).getCoordinate();
	}

	public float getValMax(Component c) {
		return getMax(c).getCoordinate();
	}
	
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		for(Component c: Component.values())
		{
			sb.append(c.name());
			sb.append(": ");
			sb.append(getValMin(c));
			sb.append(" - ");
			sb.append(getValMax(c));
			sb.append("\n");
		}
		return sb.toString();
	}
	
	public TComponentPair getClosestCollisionFromWithin(Ray ray)
	{
		float closestT = Float.POSITIVE_INFINITY;
		Component closestComponent = null;
		
		for(Component c: Component.values())
		{
			float t;
			
			if(ray.getDirection().get(c) > 0)
			{
				t = getMax(c).getCollisionT(ray);
			}
			else
			{
				t = getMin(c).getCollisionT(ray);
			}
			
			if(t < closestT && t >= 0 )
			{
				closestT = t;
				closestComponent = c;
			}
		}
		
		return new TComponentPair(closestT, closestComponent);
	}
}
