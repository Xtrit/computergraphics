package renderer.geometry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import renderer.material.Material;
import util.Matrix4f;
import util.Point3f;
import util.TexCoord2f;
import util.Tuple3f;

public class GeometryBuilder {

	private Map	<String, Shape> shapes = new HashMap<String, Shape>();
	private List<RenderableObject> renderableObjects = new ArrayList<RenderableObject>();
	private List<Renderable> renderables = new ArrayList<Renderable>();
	
	public void createSphereShape(float radius, String name)
	{
		Shape shape = new Shape();
		
		shape.addRenderable(new SphereRenderable(radius, new Tuple3f()));
		
		shapes.put(name, shape);
	}

	public void createObjects(String geometryName, Material material, Matrix4f transformationMatrix)
	{
		List<Renderable> transformedRenderables = getShape(geometryName).getTransformedRenderables(transformationMatrix);
	
		renderables.addAll(transformedRenderables);
		
		RenderableObject object = new RenderableObject(material, transformedRenderables);
		renderableObjects.add(object);
	}
	
	protected Shape getShape(String name)
	{
		return shapes.get(name);
	}

	public List<Renderable> getAllRenderables() {
		return renderables;
	}

	public void createTriangleSet(Point3f[] coordinates, Tuple3f[] normals,
			TexCoord2f[] textureCoordinates, int[] coordinateIndices,
			int[] normalIndices, int[] textureCoordinateIndices, String name) {
		
		Shape shape = new Shape();
		
		int triangles = coordinateIndices.length / 3;
		for(int i = 0; i < triangles; i++)
		{
			Triangle triangle = new Triangle(coordinates[coordinateIndices[3 * i] - 1],
					coordinates[coordinateIndices[3 * i + 1] - 1],
					coordinates[coordinateIndices[3 * i + 2] - 1]);
			
			shape.addRenderable(triangle);
		}
		
		shapes.put(name, shape);
	}
}
