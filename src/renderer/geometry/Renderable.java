package renderer.geometry;

import renderer.CollisionPoint;
import renderer.Ray;
import util.Matrix4f;

public abstract class Renderable {

	public void setObject(RenderableObject object)
	{
		this.object = object;
	}
	
	public abstract CollisionPoint getCollision(Ray ray, float maxT);
	
	public RenderableObject getObject()
	{
		return object;
	}
	
	private RenderableObject object;
	
	public abstract Renderable getTransformedRenderable(Matrix4f transformation);

	public abstract boolean ignoreSignNormal();
	
	public abstract BoundingBox calculateBoundingBox();
}
