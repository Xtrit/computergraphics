package renderer.geometry;

import java.util.List;

import renderer.CollisionPoint;
import renderer.Ray;
import renderer.Renderer;
import renderer.Statistics;
import renderer.light.Light;
import renderer.material.Material;
import util.Color3f;

public class RenderableObject
{
	public RenderableObject(Material material, List<Renderable> renderables)
	{
		this.material = material;
		this.renderables = renderables;
		
		for(Renderable renderable: renderables)
			renderable.setObject(this);
	}
	
	public Color3f handleCollision(CollisionPoint collision, List<Light> lights, Renderer renderer, Ray rayOrigin, int depth, boolean inMaterial, Statistics statistics)
	{
		return material.handleCollision(collision, lights, renderer, rayOrigin, depth, inMaterial, statistics);
	}
	
	public List<Renderable> getRenderables()
	{
		return renderables;
	}
	
	private List<Renderable> renderables;
	
	private Material material;
}
