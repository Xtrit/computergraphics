package renderer.geometry;

import java.util.ArrayList;
import java.util.List;


import util.Matrix4f;

public class Shape {
	
	public void addRenderable(Renderable renderable)
	{
		renderables.add(renderable);
	}
	
	private List<Renderable> renderables = new ArrayList<Renderable>();

	public List<Renderable> getTransformedRenderables(Matrix4f transformationMatrix)
	{
		List<Renderable> transformed = new ArrayList<Renderable>(renderables.size());
		
		for(Renderable renderable: renderables)
		{
			transformed.add(renderable.getTransformedRenderable(transformationMatrix));
		}
		
		return transformed;
	}
	
}
