package renderer.geometry;

import renderer.CollisionPoint;
import renderer.Ray;
import renderer.collision.Component;
import util.Matrix4f;
import util.Tuple3f;

public class SphereRenderable extends Renderable {

	public SphereRenderable(float radius, Tuple3f position) {
		this.radius = radius;
		this.position = position;
	}

	@Override
	public CollisionPoint getCollision(Ray ray, float maxT) {
		//throw new NotImplementedException(); //TODO Implement
		//System.out.println(maxT);
		float a = ray.getDirection().dot(ray.getDirection());
		float b = ray.getDirection().dot(ray.getOrigin().min(position));
		
		Tuple3f cMinusE = position.min(ray.getOrigin());
		
		float c = cMinusE.dot(cMinusE) - (radius * radius);
		
		float determinant = b * b - a * c;

		if(determinant < 0)
			return null;
		
		
		float sqrtDeterminant = (float) Math.sqrt(determinant);
		
		float t1 = (- b + sqrtDeterminant) / a;
		float t2 = (- b - sqrtDeterminant) / a;
		
		float smallest = Math.min(t1, t2);
		float biggest = Math.max(t1, t2);
		
		
		if(smallest < 0)
		{
			if(biggest < 0)
			{
				return null;
			}
			else
			{
				smallest = biggest;
			}
		}
		
		if(smallest > maxT)
			return null;
		
		Tuple3f collisionPoint = ray.getOrigin().add(ray.getDirection().mul(smallest));	
		Tuple3f normal = collisionPoint.min(position).normalize();
		
		CollisionPoint collision = new CollisionPoint(collisionPoint, this, null, smallest, normal);
		
		return collision;
	}

	private Tuple3f position;
	private float radius;
	
	@Override
	public Renderable getTransformedRenderable(Matrix4f transformation) {
		return new SphereRenderable(radius, transformation.mul(position));
	}

	@Override
	public boolean ignoreSignNormal() {
		return false;
	}

	@Override
	public BoundingBox calculateBoundingBox()
	{
		BoundingBox box = new BoundingBox();
			
		box.setMin(position.minAll(radius));
		box.setMax(position.addAll(radius));
		
		return box;
	}

}
