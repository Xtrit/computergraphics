package renderer.geometry;

import java.util.Collections;

import renderer.CollisionPoint;
import renderer.Ray;

import util.CramerSolver;
import util.Matrix4f;
import util.Tuple3f;
import util.ZeroDenominatorException;

public class Triangle extends Renderable {

	public Triangle(Tuple3f a, Tuple3f b, Tuple3f c)
	{
		this.a = a;
		this.b = b;
		this.c = c;
		
		calculateNormal();
	}
	
	private void calculateNormal()
	{
		normal = a.min(b).cross(a.min(c)).normalize();
	}
	
	private Tuple3f normal;
	
	private Tuple3f a;
	private Tuple3f b;
	private Tuple3f c;
	
	@Override
	public CollisionPoint getCollision(Ray ray, float maxT) {
		
		try{
			CramerSolver solver = new CramerSolver(a.min(b), a.min(c), ray.getDirection(), a.min(ray.getOrigin()));
			
			float t = solver.calculateZ();
			
			if(t < 0 || t > maxT)
				return null;
			
			float gamma = solver.calculateY();
			
			if(gamma < 0 || gamma > 1)
				return null;
			
			float beta = solver.calculateX();
			
			if(beta < 0 || beta > (1f - gamma))
				return null;
			
			Tuple3f collisionPoint = ray.getOrigin().add(ray.getDirection().mul(t));
			
			Tuple3f bary = new Tuple3f(1f - beta - gamma, beta, gamma);
			
			return new CollisionPoint(collisionPoint, this, bary, t, normal);
		
		} catch(ZeroDenominatorException e)
		{
			return null;
		}
		
	}

	@Override
	public Renderable getTransformedRenderable(Matrix4f transformation) {
		Tuple3f newA = transformation.mul(a);
		Tuple3f newB = transformation.mul(b);
		Tuple3f newC = transformation.mul(c);
		
		return new Triangle(newA, newB, newC);
	}

	@Override
	public boolean ignoreSignNormal() {
		return true;
	}

	@Override
	public BoundingBox calculateBoundingBox() {
		BoundingBox box = new BoundingBox();
		
		box.expand(a);
		box.expand(b);
		box.expand(c);
		
		return box;
	}
	
}
