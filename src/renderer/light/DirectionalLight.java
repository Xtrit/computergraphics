package renderer.light;

import renderer.NotImplementedException;
import util.Color3f;
import util.Tuple3f;

public class DirectionalLight extends Light {

	private Tuple3f direction;
	
	public DirectionalLight(Tuple3f direction, float intensity, Color3f color) {
		super(color.mul(intensity));
		
		this.direction = direction;
		
		throw new NotImplementedException(); //TODO Implement
	}

	@Override
	public Tuple3f getRayVector(Tuple3f position) {
		return direction;
	}

	@Override
	public float getDistanceRay(Tuple3f position) {
		return Float.POSITIVE_INFINITY;
	}

}
