package renderer.light;

import util.Tuple3f;

public abstract class Light {
	
	private Tuple3f color;

	Light(Tuple3f color)
	{
		this.color = color;
	}
	
	
	
	public abstract Tuple3f getRayVector(Tuple3f position);

	public Tuple3f getColor() {
		return color;
	}



	public abstract float getDistanceRay(Tuple3f position);
	
}
