package renderer.light;

import renderer.scene.Builder;
import util.Color3f;
import util.Point3f;
import util.Tuple3f;

public class LightBuilder extends Builder<Light> {

	public PointLight createPointLight(Point3f position, float intensity,
			Color3f color, String name) {
		
		PointLight light = new PointLight(position, intensity, color);
		register(light, name);
		
		return light;
	}

	public DirectionalLight createDirectionalLight(Tuple3f direction, float intensity,
			Color3f color, String name) {
		
		DirectionalLight light = new DirectionalLight(direction, intensity, color);
		register(light, name);
		
		return light;
	}

}
