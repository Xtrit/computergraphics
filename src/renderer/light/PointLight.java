package renderer.light;

import util.Color3f;
import util.Point3f;
import util.Tuple3f;

public class PointLight extends Light {

	private Tuple3f position;
	
	public PointLight(Point3f position, float intensity, Color3f color) {
		super(color.mul(intensity));
		
		this.position = position;
		
		//throw new NotImplementedException(); //TODO Implement
	}

	@Override
	public Tuple3f getRayVector(Tuple3f position) {
		return position.min(this.position);
	}

	@Override
	public float getDistanceRay(Tuple3f position) {
		return position.min(this.position).length();
	}

}
