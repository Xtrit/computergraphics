package renderer.material;

import java.util.List;

import renderer.CollisionPoint;
import renderer.Ray;
import renderer.Renderer;
import renderer.Statistics;
import renderer.light.Light;
import util.Color3f;

public class CombinedMaterial extends Material {

	Material material1, material2;
	float weight1, weight2;
	
	public CombinedMaterial(Material material1, float weight1, Material material2, float weight2)
	{
		this.material1 = material1;
		this.material2 = material2;
		this.weight1 = weight1;
		this.weight2 = weight2;
	}
	
	@Override
	public Color3f handleCollision(CollisionPoint collision, List<Light> lights, Renderer renderer, Ray ray, int depth, boolean inMaterial, Statistics statistics) {
		return new Color3f(material1.handleCollision(collision, lights, renderer, ray, depth, inMaterial, statistics).mul(weight1)
				.add(
				material2.handleCollision(collision, lights, renderer, ray, depth, inMaterial, statistics).mul(weight2)
				));
	}

}
