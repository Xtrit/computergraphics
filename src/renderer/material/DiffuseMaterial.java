package renderer.material;

import java.util.List;

import renderer.CollisionPoint;
import renderer.Ray;
import renderer.Renderer;
import renderer.light.Light;

import util.Color3f;
import util.Tuple3f;

public class DiffuseMaterial extends PhongDiffuseCommonMaterial {

	public DiffuseMaterial(Color3f color) {
		this.color = color;
	}

	private Tuple3f color;
	
	public Tuple3f calculateColor(Tuple3f normal, Tuple3f lightVector, Tuple3f lightColor, boolean ignoreSignNormal, Tuple3f viewVector)
	{
		float dotProduct = normal.dot(lightVector);
		
		if(dotProduct < 0)
		{
			if(ignoreSignNormal)
			{
				dotProduct = -dotProduct;
			}
			else
			{
				return new Tuple3f();
			}
		}
			
		return lightColor.mul(this.color).mul(dotProduct);
	}

}
