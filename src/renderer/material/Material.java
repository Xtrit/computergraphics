package renderer.material;

import java.util.List;

import renderer.CollisionPoint;
import renderer.Ray;
import renderer.Renderer;
import renderer.Statistics;
import renderer.light.Light;
import util.Color3f;

public abstract class Material {
	public abstract Color3f handleCollision(CollisionPoint collision, List<Light> lights, Renderer renderer, Ray ray, int depth, boolean inMaterial, Statistics statistics);
}
