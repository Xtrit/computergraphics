package renderer.material;

import renderer.NotImplementedException;
import renderer.scene.Builder;
import util.Color3f;

public class MaterialBuilder extends Builder<Material> {

	
	public DiffuseMaterial createDiffuseMaterial(Color3f color, String name)
	{
		DiffuseMaterial material = new DiffuseMaterial(color);
		register(material, name);
		
		return material;
	}
	
	public PhongMaterial createPhongMaterial(Color3f color, float shininess, String name)
	{
		PhongMaterial material = new PhongMaterial(color, shininess);
		register(material, name);
		
		return material;
	}

	public CombinedMaterial createCombinedMaterial(Material material1, float weight1,
			Material material2, float weight2, String name) {
		CombinedMaterial material = new CombinedMaterial(material1, weight1, material2, weight2);
		register(material, name);
		
		return material;
	}

	public ReflectionMaterial createReflectionMaterial(String name) {
		ReflectionMaterial material = new ReflectionMaterial();
		
		register(material, name);
		
		return material;
	}

	public RefractionMaterial createRefractionMaterial(float index, String name) {
		RefractionMaterial material = new RefractionMaterial(index);
		
		register(material, name);
		
		return material;
	}
}
