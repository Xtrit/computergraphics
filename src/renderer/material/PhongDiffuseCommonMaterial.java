package renderer.material;

import java.util.List;

import renderer.CollisionPoint;
import renderer.Ray;
import renderer.Renderer;
import renderer.Statistics;
import renderer.light.Light;
import util.Color3f;
import util.Tuple3f;

public abstract class PhongDiffuseCommonMaterial extends Material {

	public PhongDiffuseCommonMaterial() {
		super();
	}

	public abstract Tuple3f calculateColor(Tuple3f normal, Tuple3f lightVector, Tuple3f lightColor, boolean ignoreSignNormal, Tuple3f viewVector);
	
	@Override
	public Color3f handleCollision(CollisionPoint collision, List<Light> lights, Renderer renderer, Ray ray, int depth, boolean inMaterial, Statistics statistics) {
		
		Tuple3f rayOrigin = ray.getOrigin();
		Tuple3f viewVector = rayOrigin.min(collision.getCollisionPoint());
		viewVector = viewVector.normalize();
		
		Tuple3f color = new Color3f();
		
		for(Light light: lights)
		{			
			Tuple3f lightVector = light.getRayVector(collision.getCollisionPoint()).reverse().normalize();
			float t = light.getDistanceRay(collision.getCollisionPoint());
			
			CollisionPoint shadowCollision = renderer.getCollision(new Ray(collision.getCollisionPoint(), lightVector, null), t, true, collision.getRenderable(), statistics);
			if(shadowCollision != null)
				continue;
			
			color = color.add(calculateColor(collision.getNormal(), lightVector, light.getColor(), collision.getRenderable().ignoreSignNormal(), viewVector));
		}
		
		return new Color3f(color);
	}
}