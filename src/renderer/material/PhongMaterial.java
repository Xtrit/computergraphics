package renderer.material;

import java.util.List;

import renderer.CollisionPoint;
import renderer.NotImplementedException;
import renderer.Renderer;
import renderer.light.Light;

import util.Color3f;
import util.Tuple3f;

public class PhongMaterial extends PhongDiffuseCommonMaterial {

	public PhongMaterial(Color3f color, float shininess) {
		this.color = color;
		this.shininess = shininess;
	}
	
	private Color3f color;
	private float shininess;

	@Override
	public Tuple3f calculateColor(Tuple3f normal, Tuple3f lightVector,
			Tuple3f lightColor, boolean ignoreSignNormal, Tuple3f viewVector) {
		
		Tuple3f h = viewVector.add(lightVector);
		
		h = h.normalize();
		
		float dotProduct = normal.dot(h);
		
		if(dotProduct < 0)
		{
			if(ignoreSignNormal)
			{
				dotProduct = -dotProduct;
			}
			else
			{
				return new Tuple3f();
			}
		}
			
		float powered = (float) Math.pow(dotProduct, shininess);
		if(powered > 0.5)
		{
			int o = 0;
		}
		return lightColor.mul(this.color).mul(powered);
		
	}





}
