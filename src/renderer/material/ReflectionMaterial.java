package renderer.material;

import java.util.List;

import renderer.CollisionPoint;
import renderer.Ray;
import renderer.Renderer;
import renderer.Statistics;
import renderer.light.Light;
import util.Color3f;
import util.Tuple3f;

public class ReflectionMaterial extends Material
{

	@Override
	public Color3f handleCollision(CollisionPoint collision, List<Light> lights, Renderer renderer, Ray ray, int depth, boolean inMaterial, Statistics statistics) {
		
		
		
		Tuple3f n = collision.getNormal();
		Tuple3f d = ray.getDirection();
		
		float dotProduct = d.dot(n);
		
		Tuple3f r = d.min(n.mul(2 * dotProduct));
		
		Ray newRay = new Ray(collision.getCollisionPoint(), r, null);
		
		return renderer.renderRay(newRay, statistics, depth - 1, inMaterial, collision.getRenderable());
	}
	

}
