package renderer.material;

import java.util.List;

import renderer.CollisionPoint;
import renderer.Ray;
import renderer.Renderer;
import renderer.Statistics;
import renderer.collision.Component;
import renderer.geometry.Renderable;
import renderer.light.Light;
import util.Color3f;
import util.Tuple3f;

public class RefractionMaterial extends Material {

	public RefractionMaterial(float index) {
		this.index = index;
		
		float ln = (float) Math.log(1);
		for(Component comp: Component.values())
		{
			a.set(comp, ln/2);
		}
	}
	
	private float index;
	private Tuple3f a = new Tuple3f();


	
	@Override
	public Color3f handleCollision(CollisionPoint collision,
			List<Light> lights, Renderer renderer, Ray ray, int depth, boolean inMaterial,
			Statistics statistics) {
		
		
		if(depth == 1)
			return new Color3f();
		
		Info info = new Info();
		info.depth = depth;
		info.exclude = collision.getRenderable();
		info.inMaterial = inMaterial;
		
		
		Tuple3f n = collision.getNormal();
		Tuple3f d = ray.getDirection();
		float tf = collision.getDistance();
		Tuple3f k = null;
		
		//Tuple3f reflect = reflect(collision, lights, renderer, ray, depth, inMaterial, statistics);
		
		float c = 0;
		
		Tuple3f t;
		
		if(!inMaterial)
		{
			t = refract(d, n, this.index);
			c = d.dot(n);
			k = new Tuple3f(1,1,1);
		}
		else
		{			
			k = new Tuple3f();
			for(Component comp: Component.values())
			{
				k.set(comp, (float) Math.exp(a.get(comp) * tf));
			}
			t = refract(d, n, 1/this.index);
			
			//Total internal refraction
			if(t == null)
			{
				//return new Color3f();
				return new Color3f(k.mul(reflect(collision, lights, renderer, ray, depth - 1, true, statistics)));
			}
			
			c = t.dot(n);
		}
		
		if(c < 0)
			c = -c;
		
		float R0 = ((this.index - 1)*(this.index - 1))/((this.index + 1)*(this.index + 1));
		float R = R0 + ((1 - R0)*(1-c)*(1-c)*(1-c)*(1-c)*(1-c));
		
		
		Tuple3f reflected = reflect(collision, lights, renderer, ray, depth - 1, inMaterial, statistics).mul(R);
		
		Renderable excl = null;
		if(collision.getRenderable().ignoreSignNormal() || !inMaterial)
			excl = collision.getRenderable();
		

		Ray newRay = new Ray(collision.getCollisionPoint(), t, null);
		Tuple3f rendered = renderer.renderRay(newRay, statistics, depth, !inMaterial, excl).mul(1-R);
		
		//return  new Color3f( rendered);
		
		if(R < 0)
			throw new IllegalStateException();
		
		return new Color3f(k.mul(reflected.add(rendered)));
	}
	
	private Color3f reflect(CollisionPoint collision,
			List<Light> lights, Renderer renderer, Ray ray, int depth, boolean inMaterial,
			Statistics statistics)
	{
		
		return reflectionMaterial.handleCollision(collision, lights, renderer, ray, depth, inMaterial, statistics);
	}
	
	private ReflectionMaterial reflectionMaterial = new ReflectionMaterial();
	
	private Tuple3f refract(Tuple3f d, Tuple3f n, float index)
	{
		float dot = d.dot(n);
		
		if(dot > 0)
		{
			dot = -dot;
			n = n.reverse();
		}
			
		float sqrt = 1 - ((1 - (dot*dot))/(index * index));
		
		if(sqrt < 0)
			return null;
		
		//(d-(n*dot))/index
		Tuple3f first = (d.min(n.mul(dot)).div(index));
		Tuple3f second = n.mul((float)Math.sqrt(sqrt));
		
		return first.min(second);
	}

	private Tuple3f color(Tuple3f p, Tuple3f r, Renderer renderer, Info info)
	{
		Ray ray = new Ray(p, r, null);
		return renderer.renderRay(ray, info.statistics, info.depth, info.inMaterial, info.exclude);
	}
	
	private class Info
	{
		public Statistics statistics;
		public int depth;
		public boolean inMaterial;
		public Renderable exclude;
	}
}
