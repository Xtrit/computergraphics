package renderer.scene;

import java.util.HashMap;
import java.util.Map;



public abstract class Builder<T> {
	
	public Builder()
	{
		registry = new HashMap<String, T>();
	}
	
	private Map<String, T> registry;
	
	protected void register(T item, String name)
	{
		this.registry.put(name, item);
	}
	
	public T get(String name)
	{
		return this.registry.get(name);
	}

}
