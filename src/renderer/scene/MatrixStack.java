package renderer.scene;

import java.util.Stack;

import util.Matrix4f;
import util.Tuple3f;

public class MatrixStack {

	private Stack<Matrix4f> stack;
	
	public MatrixStack()
	{
		stack = new Stack<Matrix4f>();
		stack.push(Matrix4f.createIdentity());
	}
	
	public void pop()
	{
		if(stack.size() <= 1)
			throw new IllegalStateException("You can not pop more matrices then you have stacked.");
		
		stack.pop();
	}
	
	public Matrix4f peek()
	{
		return stack.peek();
	}
	
	public void translate(Tuple3f vector)
	{
		Matrix4f transMatrix = new Matrix4f();
		
		transMatrix.setIdentity();
		transMatrix.m03 = vector.x;
		transMatrix.m13 = vector.y;
		transMatrix.m23 = vector.z;
		
		multiplyAndStack(transMatrix);
	}
	
	private void multiplyAndStack(Matrix4f transMatrix)
	{
		//M_new = M_trans * M_old
		//push(M_new)
		push(transMatrix.mul(peek()));
	}
	
	private void push(Matrix4f matrix)
	{
		stack.push(matrix);
	}
	
	public void scale(Tuple3f factor)
	{
		Matrix4f transMatrix = new Matrix4f();
		
		transMatrix.m00 = factor.x;
		transMatrix.m22 = factor.z;
		transMatrix.m11 = factor.y;
		transMatrix.m33 = 1;
		
		multiplyAndStack(transMatrix);
	}
	
	public void rotate(Tuple3f axis, float angle)
	{
		float inRad = (float)Math.toRadians(angle);
		float cos = (float)Math.cos(inRad);
		float sin = (float)Math.sin(inRad);
		
		Matrix4f transMatrix = new Matrix4f();
		
		transMatrix.setIdentity();
		
		Tuple3f axisNormalized = axis.normalize();
		
		transMatrix.m00 = cos + axisNormalized.x * axisNormalized.x * (1 - cos);
		transMatrix.m10 = (1 - cos) * axisNormalized.y * axisNormalized.x + axisNormalized.z * sin;
		transMatrix.m20 = (1 - cos) * axisNormalized.z * axisNormalized.x - axisNormalized.y * sin;
		
		transMatrix.m01 = (1 - cos) * axisNormalized.y * axisNormalized.x - axisNormalized.z * sin;
		transMatrix.m11 = cos + axisNormalized.y * axisNormalized.y * (1 - cos);
		transMatrix.m21 = (1 - cos) * axisNormalized.y * axisNormalized.z + axisNormalized.x * sin;
		
		transMatrix.m02 = (1 - cos) * axisNormalized.z * axisNormalized.x + axisNormalized.y * sin;
		transMatrix.m12 = (1 - cos) * axisNormalized.y * axisNormalized.z - axisNormalized.x * sin;
		transMatrix.m22 = cos + axisNormalized.z * axisNormalized.z * (1 - cos);
		
		multiplyAndStack(transMatrix);
	}
}
