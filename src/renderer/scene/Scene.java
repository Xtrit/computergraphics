package renderer.scene;

import java.util.ArrayList;
import java.util.List;

import renderer.Camera;
import renderer.NotImplementedException;
import renderer.geometry.Renderable;
import renderer.light.Light;
import util.Color3f;

public class Scene {
	
	private Camera camera;
	private List<Light> lights;
	private Color3f backgroundColor;
	private List<Renderable> renderables = new ArrayList<Renderable>();

	public Scene(Camera camera, List<Light> lights, Color3f backgroundColor)
	{
		this.camera = camera;
		this.lights = new ArrayList<Light>(lights);
		this.backgroundColor = backgroundColor;
	}
	
	public Scene() {
	}

	public Camera getCamera()
	{
		return camera;
	}

	public Color3f getBackgroundColor() {
		return backgroundColor;
	}

	public List<Light> getLights() {
		return lights;
	}

	public List<Renderable> getRenderables() {
		return renderables;
	}

	public void addRenderables(List<Renderable> toAdd) {
		
		renderables.addAll(toAdd);
	}
	
}
