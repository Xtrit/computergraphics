package renderer.scene;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.xml.sax.InputSource;

import renderer.CameraBuilder;
import renderer.NotImplementedException;
import renderer.geometry.GeometryBuilder;
import renderer.light.Light;
import renderer.light.LightBuilder;
import renderer.material.MaterialBuilder;

import util.Color3f;
import util.Parser;
import util.ParserHandler;
import util.Point3f;
import util.TexCoord2f;
import util.Tuple2f;
import util.Tuple3f;

/**
  * Class used to build a scene from a given sdl file.
  * Implements the ParserHandler interface (these methods
  * need to be filled in by you).
  * 
  * Note that this class keeps the absolute path to the
  * directory where the sdl file was found.  If you put your
  * textures in the same directory, you can use this path
  * to construct an absolute file name for each texture.
  * You will probably need absolute file names when loading
  * the texture.
  */
public class SceneBuilder implements ParserHandler
{

    // the scene being build
    private Scene scene = null;

    private Scene getScene() { return scene; }

    // the path to the xml directory
    // this path can be used to put in front of the texture file name
    // to load the textures
    private String path = null;
	private Tuple2f resolution;

    public String getPath() { return path; }


    /**
     * Load a scene.
     * @param filename The name of the file that contains the scene.
     * @param resolution 
     * @return The scene, or null if something went wrong.
     * @throws FileNotFoundException The file could not be found.
     */
    public Scene loadScene(String filename, Tuple2f resolution) throws FileNotFoundException
    {
        // create file and file input stream
        File file = new File(filename);
        FileInputStream fileInputStream = new FileInputStream(file);
        
        this.resolution = resolution;

        // set the system id so that the dtd can be a relative path
        // the first 2 lines of your sdl file should always be
        //    <?xml version='1.0' encoding='utf-8'?>
        //    <!DOCTYPE Sdl SYSTEM "sdl.dtd">
        // and sdl.dtd should be in the same directory as the dtd
        // if you experience dtd problems, commend the doctype declaration
        //    <!-- <!DOCTYPE Sdl SYSTEM "sdl.dtd"> -->
        // and disable validation (see further)
        // although this is in general not a good idea

        InputSource inputSource = new InputSource(fileInputStream);
        String parentPath = file.getParentFile().getAbsolutePath() + "/";
        path = file.getParentFile().getAbsolutePath() + "/";
        inputSource.setSystemId("file:///" + file.getParentFile().getAbsolutePath() + "/");



        // create the new scene
        scene = new Scene();

        // create the parser and parse the input file
        Parser parser = new Parser();
        parser.setHandler(this);

        // if the output bothers you, set echo to false
        // also, if loading a large file (with lots of triangles), set echo to false
        // you should leave validate to true
        // if the docuement is not validated, the parser will not detect syntax errors
        if (parser.parse(inputSource, /* validate */ true, /* echo */ true) == false)
        {
            scene = null;
        }

        // return the scene
        return scene;
    }

    /*
     *  (non-Javadoc)
     * ParserHandler callbacks
     */	
    
    private CameraBuilder cameraBuilder = new CameraBuilder();
    private MaterialBuilder materialBuilder = new MaterialBuilder();
    private LightBuilder lightBuilder = new LightBuilder();
    private MatrixStack matrixStack = new MatrixStack();
    private GeometryBuilder geometryBuilder = new GeometryBuilder();

    public void startSdl() throws Exception
    {
    }

    public void endSdl() throws Exception
    {
    }

    public void startCameras() throws Exception
    {
    }

    public void endCameras() throws Exception
    {
    }

    public void startCamera(Tuple3f position, Tuple3f direction, Tuple3f up, float fovy, String name) throws Exception
    {
    	cameraBuilder.create(resolution, position, direction, up, fovy, name);
    }

    public void endCamera() throws Exception
    {
    }

    public void startLights() throws Exception
    {
    }

    public void endLights() throws Exception
    {
    }

    public void startDirectionalLight(Tuple3f direction, float intensity, Color3f color, String name) throws Exception
    {
    	lightBuilder.createDirectionalLight(direction, intensity, color, name);
    }

    public void endDirectionalLight() throws Exception
    {
    }

    public void startPointLight(Point3f position, float intensity, Color3f color, String name) throws Exception
    {
    	lightBuilder.createPointLight(position, intensity, color, name);
    }

    public void endPointLight() throws Exception
    {
    }

    public void startSpotLight(Point3f position, Tuple3f direction, float angle, float intensity, Color3f color, String name) throws Exception
    {
    	throw new NotImplementedException(); //TODO Implement
    }

    public void endSpotLight() throws Exception
    {
    }

    public void startGeometry() throws Exception
    {
    }

    public void endGeometry() throws Exception
    {
    }

    public void startSphere(float radius, String name) throws Exception
    {
    	geometryBuilder.createSphereShape(radius, name);
    	//throw new NotImplementedException(); //TODO Implement
    }

    public void endSphere() throws Exception
    {
    }

    public void startCylinder(float radius, float height, boolean capped, String name) throws Exception
    {
    	throw new NotImplementedException(); //TODO Implement
    }

    public void endCylinder() throws Exception
    {
    }

    public void startCone(float radius, float height, boolean capped, String name) throws Exception
    {
    	throw new NotImplementedException(); //TODO Implement
    }

    public void endCone() throws Exception
    {
    }

    public void startTorus(float innerRadius, float outerRadius, String name) throws Exception
    {
    	throw new NotImplementedException(); //TODO Implement
    }

    public void endTorus() throws Exception
    {
    }

    public void startTeapot(float size, String name) throws Exception
    {
    	throw new NotImplementedException(); //TODO Implement
    }

    public void endTeapot() throws Exception
    {
    }

    public void startIndexedTriangleSet(Point3f [] coordinates, Tuple3f [] normals, TexCoord2f [] textureCoordinates, int [] coordinateIndices, int [] normalIndices, int [] textureCoordinateIndices, String name) throws Exception
    {
    	geometryBuilder.createTriangleSet(coordinates, normals, textureCoordinates, coordinateIndices, normalIndices, textureCoordinateIndices, name);
    }

    public void endIndexedTriangleSet() throws Exception
    {
    }

    public void startTextures() throws Exception
    {
    }

    public void endTextures() throws Exception
    {
    }

    public void startTexture(String src, String name) throws Exception
    {
    	throw new NotImplementedException(); //TODO Implement
    }

    public void endTexture() throws Exception
    {
    }

    public void startMaterials() throws Exception
    {
    }

    public void endMaterials() throws Exception
    {
    }

    public void startDiffuseMaterial(Color3f color, String name) throws Exception
    {
    	materialBuilder.createDiffuseMaterial(color, name);
    }

    public void endDiffuseMaterial() throws Exception
    {
    }

    public void startPhongMaterial(Color3f color, float shininess, String name) throws Exception
    {
    	materialBuilder.createPhongMaterial(color, shininess, name);
    }

    public void endPhongMaterial() throws Exception
    {
    }

    public void startLinearCombinedMaterial(String material1Name, float weight1, String material2Name, float weight2, String name) throws Exception
    {
    	materialBuilder.createCombinedMaterial(materialBuilder.get(material1Name), weight1,materialBuilder.get(material2Name), weight2, name);
    }

    public void endLinearCombinedMaterial() throws Exception
    {
    }

    public void startScene(String cameraName, String [] lightNames, Color3f background) throws Exception
    {
    	List<Light> lights = new ArrayList<Light>(lightNames.length);
    	
    	for(String lightName: lightNames)
    	{
    		lights.add(lightBuilder.get(lightName));
    	}
    	
    	scene = new Scene(
    		cameraBuilder.get(cameraName),
    		lights,
    		background
    			);
    }

    public void endScene() throws Exception
    {
    	scene.addRenderables(geometryBuilder.getAllRenderables());
    }

    public void startShape(String geometryName, String materialName, String textureName) throws Exception
    {
    	geometryBuilder.createObjects(geometryName, materialBuilder.get(materialName), matrixStack.peek());
    }

    public void endShape() throws Exception
    {
    }

    public void startRotate(Tuple3f axis, float angle) throws Exception
    {
    	matrixStack.rotate(axis, angle);
    }

    public void endRotate() throws Exception
    {
    	matrixStack.pop();
    }

    public void startTranslate(Tuple3f vector) throws Exception
    {
    	matrixStack.translate(vector);
    }

    public void endTranslate() throws Exception
    {
    	matrixStack.pop();
    }

    public void startScale(Tuple3f scale) throws Exception
    {
    	matrixStack.scale(scale);
    }

    public void endScale() throws Exception
    {
    	matrixStack.pop();
    }


	@Override
	public void startReflectionMaterial(String name) {
		materialBuilder.createReflectionMaterial(name);		
	}


	@Override
	public void endReflectionMaterial() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void startRefractionMaterial(float index, String name) {
		materialBuilder.createRefractionMaterial(index, name);
		
	}


	@Override
	public void endRefractionMaterial() {
		// TODO Auto-generated method stub
		
	}

}
