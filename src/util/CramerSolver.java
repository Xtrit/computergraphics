package util;

public class CramerSolver {

	/**
	 * 
	 * @param col1
	 * @param col2
	 * @param col3
	 * @param x
	 * 
	 * @throws	ZeroDenominatorException
	 * 			|col1 col2 col3| = 0 
	 */
	public CramerSolver(Tuple3f col1, Tuple3f col2, Tuple3f col3, Tuple3f x) {
		// TODO Auto-generated constructor stub
		
		this.col1 = col1;
		this.col2 = col2;
		this.col3 = col3;
		
		this.x = x;
		
		Matrix3f matrix = new Matrix3f(col1, col2, col3);
		
		denominator =  matrix.getDeterminant();
		
		if(denominator == 0f)
			throw new ZeroDenominatorException();
	}
	
	private float denominator;
	private Tuple3f col1;
	private Tuple3f col2;
	private Tuple3f col3;
	private Tuple3f x;
	
	
	public float calculateX()
	{
		Matrix3f matrix = new Matrix3f(x, col2, col3);
		return matrix.getDeterminant()/denominator;
	}
	
	public float calculateY()
	{
		Matrix3f matrix = new Matrix3f(col1, x, col3);
		return matrix.getDeterminant()/denominator;
	}
	
	public float calculateZ()
	{
		Matrix3f matrix = new Matrix3f(col1, col2, x);
		return matrix.getDeterminant()/denominator;
	}

}
