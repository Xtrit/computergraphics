package util;

public class Matrix3f {

	protected float	m00, m01, m02,
						m10, m11, m12,
						m20, m21, m22;
	
	public Matrix3f(Tuple3f col1, Tuple3f col2, Tuple3f col3)
	{
		m00 = col1.x;
		m10 = col1.y;
		m20 = col1.z;
		
		m01 = col2.x;
		m11 = col2.y;
		m21 = col2.z;
		
		m02 = col3.x;
		m12 = col3.y;
		m22 = col3.z;
	}
	
	public float getDeterminant()
	{
		//Sarrus		
		return m00*m11*m22 + m01*m12*m20 + m02*m10*m21
				-m02*m11*m20 - m01*m10*m22 - m00*m12*m21;
	}
	
	public String toString()
	{
		return m00 + "\t" + m01 + "\t" + m02 + "\n" +
				m10 + "\t" + m11 + "\t" + m12 + "\n" +
				m20 + "\t" + m21 + "\t" + m22 + "\n";
	}
}
