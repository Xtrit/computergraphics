package util;
/**
 * A generic 3-element tuple that is represented by
 * single precision-floating point x,y,z coordinates.
 * 
 *
 *
 */

import java.io.Serializable;

import renderer.collision.Component;


public class Tuple3f implements Serializable, Cloneable
{
    /**
     * The x coordinate.
     */

    public float x;


    /**
     * The y coordinate.
     */

    public float y;


    /**
     * The z coordinate.
     */

    public float z;

    /**
     * Constructs and initializes a Tuple3f to (0,0,0).
     */

    public Tuple3f()
    {
        x = 0.0F;
        y = 0.0F;
        z = 0.0F;
    }


    /**
     * Constructs and initializes a Tuple3f from the specified xyz coordinates.
     * @param x
     * @param y
     * @param z
     */ 

    public Tuple3f(float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }


    /**
     * Constructs and initializes a Tuple3f from the array of length 3. 
     * @param t
     */

    public Tuple3f(float t[])
    {
        x = t[0];
        y = t[1];
        z = t[2];
    }


    /**
     * Constructs and initializes a Tuple3f from the specified Tuple3d.
     * @param t
     */

    public Tuple3f(Tuple3f t)
    {
        x = t.x;
        y = t.y;
        z = t.z;
    }


    /**
     * Returns a string that contains the values of this Tuple3f 
     */

    public String toString()
    {
        return "(" + x + ", " + y + ", " + z + ")";
    }


    /**
     *  Sets the value of this tuple to the specified xyz coordinates.
     * @param x
     * @param y
     * @param z
     */

    public final void set(float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }


    /**
     * Sets the value of this tuple to the xyz coordinates specified in the array of length 3.
     * @param t
     */

    public final void set(float t[])
    {
        x = t[0];
        y = t[1];
        z = t[2];
    }


    /**
     * Sets the value of this tuple to the value of tuple t1.
     * @param t
     */

    public final void set(Tuple3f t)
    {
        x = t.x;
        y = t.y;
        z = t.z;
    }


    /**
     *   Gets the value of this tuple and copies the values into t.
     * @param t
     */

    public final void get(float t[])
    {
        t[0] = x;
        t[1] = y;
        t[2] = z;
    }


    /**
     * Gets the value of this tuple and copies the values into t.
     * @param t
     */

    public final void get(Tuple3f t)
    {
        t.x = x;
        t.y = y;
        t.z = z;
    }


    /**
     *  Creates a new object of the same class as this object.
     */	

    public Object clone()
    {
        try
        {
            return super.clone();
        }
        catch(CloneNotSupportedException clonenotsupportedexception)
        {
            throw new InternalError();
        }
    }
    
    public Tuple3f add(Tuple3f toAdd)
    {
    	return new Tuple3f(this.x + toAdd.x, this.y + toAdd.y, this.z + toAdd.z);
    }
    
    public Tuple3f mul(float scalar)
    {
    	return new Tuple3f(this.x * scalar, this.y * scalar, this.z * scalar);
    }
    
    public Tuple3f mul(Tuple3f toMul)
    {
    	return new Tuple3f(this.x * toMul.x, this.y * toMul.y, this.z * toMul.z);
    }
    
    public Tuple3f div(Tuple3f toDiv)
    {
    	return new Tuple3f(this.x / toDiv.x, this.y/toDiv.y, this.z/toDiv.z);
    }
    
    public Tuple3f cross(Tuple3f other)
    {
    	return new Tuple3f(this.y * other.z - this.z * other.y,
    			this.z * other.x - this.x * other.z,
    			this.x * other.y - this.y * other.x);
    }

    public float dot(Tuple3f other)
    {
    	return this.x * other.x + this.y * other.y + this.z * other.z;
    }
    
	public Tuple3f min(Tuple3f other) {
		return new Tuple3f(this.x - other.x, this.y - other.y, this.z - other.z);
	}
	
	public float length()
	{
		return (float) Math.sqrt(x * x + y * y + z * z);
	}


	public Vector3f normalize() {
		float length = length();
		
		return new Vector3f(this.x / length, this.y / length, this.z / length);
	}


	public Tuple3f reverse() {
		return new Tuple3f(-x, -y, -z);
	}


	public Tuple3f minAll(float min) {
		return new Tuple3f(x - min, y - min, z - min);
	}


	public Tuple3f addAll(float add) {
		return new Tuple3f(x + add, y + add, z + add);
	}

	public void set(Component c, float value)
	{
		c.setComponent(this, value);
	}
	
	public float get(Component c)
	{
		return c.getComponent(this);
	}


	public Tuple3f div(int toDivWith) {
		return new Tuple3f(x/toDivWith, y/toDivWith, z/toDivWith);
	}
	
	public Tuple3f div(float toDivWith) {
		return new Tuple3f(x/toDivWith, y/toDivWith, z/toDivWith);
	}
}
