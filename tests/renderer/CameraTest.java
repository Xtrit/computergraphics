package renderer;

import static org.junit.Assert.*;

import org.junit.Test;

import util.Point2f;
import util.Tuple2f;
import util.Tuple3f;
import util.Vector3f;

public class CameraTest {

	@Test
	public void test5x5() {
		/*
		 * We kijken vanuit 0 volgens de x-as
		 */
		Camera c = new Camera(new Tuple2f(5,5), new Vector3f(), new Vector3f(1,0,0), 90, new Vector3f(0, 0, 1));
	
		for(int x = 0; x < 5; x++)
			for(int y = 0; y < 5; y++)
				testPixel5x5(new Tuple2f(x, y), c);
	}

	private void testPixel5x5(Tuple2f point, Camera c)
	{
		float expectedX = 1.0f;
		float expectedZ = (point.y - 2.0f) * 0.4f;
		float expectedY = (-(point.x - 2.0f)) * 0.4f;
		
		Tuple3f expected = new Tuple3f(expectedX, expectedY, expectedZ);
		expected = expected.normalize();
		
		Ray r = c.createRayForPixel(point);
		assertEquals(expected.x, r.getDirection().x, 0.0001f);
		assertEquals(expected.y, r.getDirection().y, 0.0001f);
		assertEquals(expected.z, r.getDirection().z, 0.0001f);
	}
	
	@Test
	public void test4x4() {
		/*
		 * We kijken vanuit 0 volgens de y-as
		 */
		Camera c = new Camera(new Point2f(4, 4), new Vector3f(), new Vector3f(0,1,0), 90, new Vector3f(0, 0, 1));
	
		for(int x = 0; x < 4; x++)
			for(int y = 0; y < 4; y++)
				testPixel4x4(new Tuple2f(x, y), c);
	}

	private void testPixel4x4(Tuple2f point, Camera c)
	{
		float expectedY = 1.0f;
		float expectedZ = (point.y - 1.5f) * 0.5f;
		float expectedX = (point.x - 1.5f) * 0.5f;
		
		Tuple3f expected = new Tuple3f(expectedX, expectedY, expectedZ);
		expected = expected.normalize();
		
		Ray r = c.createRayForPixel(point);
		assertEquals(expected.x, r.getDirection().x, 0.0001f);
		assertEquals(expected.y, r.getDirection().y, 0.0001f);
		assertEquals(expected.z, r.getDirection().z, 0.0001f);
	}
}
