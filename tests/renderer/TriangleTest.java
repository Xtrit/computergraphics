package renderer;

import static org.junit.Assert.*;

import org.junit.Test;

import renderer.geometry.Triangle;

import util.Tuple2f;
import util.Tuple3f;

public class TriangleTest {

	@Test
	public void testSimple() {
		Triangle hit = new Triangle(
				new Tuple3f(1, -1, -1),
				new Tuple3f(1, 0, 2),
				new Tuple3f(1, 1, -1)
		);
		Triangle missAbove = new Triangle(
				new Tuple3f(1, -1, 2),
				new Tuple3f(1, 0, 5),
				new Tuple3f(1, 1, 2)
		);
		Ray ray = new Ray(new Tuple3f(), new Tuple3f(1, 0, 0), new Tuple2f());
		
		CollisionPoint cp = hit.getCollision(ray, Float.POSITIVE_INFINITY);
		assertNotNull(cp);
		
		assertEquals(cp.getCollisionPoint().x, 1f, 0.000001);
		assertEquals(cp.getCollisionPoint().y, 0f, 0.000001);
		assertEquals(cp.getCollisionPoint().z, 0f, 0.000001);
		
		assertEquals(cp.getRenderable(), hit);
		
		assertNull(missAbove.getCollision(ray, Float.POSITIVE_INFINITY));
	}

}
