package scene;

import static org.junit.Assert.*;

import org.junit.Test;

import renderer.scene.MatrixStack;

import util.Tuple3f;
import util.Vector3f;

public class MatrixStackTest {

	@Test
	public void testRotationSimple()
	{
		Tuple3f point = new Vector3f(1, 0, 0);
		Tuple3f axis = new Vector3f(0, 0, 1);
		float angle = 90;
		
		MatrixStack stack = new MatrixStack();
		stack.rotate(axis, angle);
		Tuple3f rotated = stack.peek().mul(point);
		
		assertEquals(0, rotated.x, 0.00001);
		assertEquals(1, rotated.y, 0.00001);
		assertEquals(0, rotated.z, 0.00001);
	}
	
	@Test
	public void testRotation() {
		Tuple3f point = new Vector3f(1, 0, 0);
		Tuple3f axis = new Vector3f(1, 1, 1);
		float angle = 90;
		
		MatrixStack stack = new MatrixStack();
		stack.rotate(axis, angle);
		Tuple3f rotated = stack.peek().mul(point);
		
		assertEquals(0, rotated.x, 0.00001);
		assertEquals(0, rotated.y, 0.00001);
		assertEquals(0, rotated.z, 0.00001);
	}

}
