package util;

import static org.junit.Assert.*;

import org.junit.Test;

public class Matrix4fTest {

	@Test
	public void testMul()
	{
		/*
		 * A =
		 * 1	4	8	7
		 * 2	2	10	3
		 * 3	4	7	0
		 * 4	5	6	8
		 * 
		 * B = 
		 * 0	4	6	8
		 * 4	5	7	9
		 * 1	4	9	6
		 * 4	2	0	3
		 * 
		 * A*B =
		 * 52	70	106	113
		 * 30	64	116	103
		 * 23	60	109	102
		 * 58	81	113	137
		 */
		
		Matrix4f a = new Matrix4f(1, 4, 8, 7, 2, 2, 10, 3, 3, 4, 7, 0, 4, 5, 6, 8);
		Matrix4f b = new Matrix4f(0, 4, 6, 8, 4, 5, 7, 9, 1, 4, 9, 6, 4, 2, 0, 3);
		Matrix4f c = new Matrix4f(52, 70, 106, 113, 30, 64, 116, 103, 23, 60, 109, 102, 58, 81, 113, 137);
		
		Matrix4f result = a.mul(b);
		
		for(int i = 0; i < 4; i++)
		{
			for(int j = 0; j < 4; j++)
			{
				assertEquals(c.getElement(i, j), result.getElement(i, j), 0.001);
			}
		}
	}
	
}
